package com.sidlors.lab.microservice.store.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.sidlors.lab.microservice.store.StoreApp;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = StoreApp.class)
@WebAppConfiguration
public class StoreAppTest {

    @Test
	public void contextLoads() {

	}

}
