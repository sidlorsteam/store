package com.sidlors.lab.microservice.store.test.repository;

import static org.junit.Assert.assertEquals;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import com.sidlors.lab.microservice.store.StoreApp;
import com.sidlors.lab.microservice.store.model.Order;
import com.sidlors.lab.microservice.store.repository.OrderRepository;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import de.flapdoodle.embed.mongo.MongodExecutable;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = StoreApp.class)
@WebAppConfiguration
public class OrderRepositoryTest {

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	TestRespository testRespository;


	@Test
	@DisplayName("Should find order by Nick ")
	public void shouldFindOrderByNick() {
		Order stub = getStubOrder();
		orderRepository.save(stub);
		Order newOrder = testRespository.add(getStubOrder());
		Order found = orderRepository.findById(newOrder.getId()).get();
		assertEquals(stub.getId(), found.getId());
	}

	/**
	 * 
	 * @return Create a new order with id 
	 */
	private Order getStubOrder() {
		Order order = new Order();
		order.setId(11L);		
		return order;
	}

	@Component
	static  class TestRespository{
		@Autowired
		private OrderRepository orderrRepository;

		/**
		 * @return list Orders 
		 */
		 public Iterable<Order> findall(){
			 return orderrRepository.findAll();
		 }

		 /**
		  * 
		  * @param order
		  * @return A order created
		  */
		 public Order add(Order order){
			 return orderrRepository.save(order);
		 }

		/**
		 * 
		 * @param Id from Order created
		 * @return order finded 
		 */
		 public Order findById(Long Id){
			 return orderrRepository.findById(Id).get();
		 }

	} 
}
