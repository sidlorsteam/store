package com.sidlors.lab.microservice.store.repository;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sidlors.lab.microservice.store.model.Order;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {

	Optional<Order> findById(Long Id);
	
	
}
