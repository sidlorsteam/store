package com.sidlors.lab.microservice.store.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.collections.IteratorUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.sidlors.lab.microservice.store.model.Order;
import com.sidlors.lab.microservice.store.repository.OrderRepository;

@Service
public class StoreServiceImpl implements StoreService {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private OrderRepository orderRepository;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Order findById(Long id) {
		Optional<Order> order = orderRepository.findById(id);
		Assert.notNull(order, "can't find order with name " + order.get().getId());
		order.get().setPetId(order.get().getPetId());
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Order create(Order order) {

		Order existing = orderRepository.findById(order.getId()).orElse(null);

		Assert.isNull(existing, "order already exists: " + order.getId());
		orderRepository.save(order);

		log.info("new order has been created: " + order.getId());

		return order;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void saveChanges(Order update) {
		Optional<Order> order = orderRepository.findById(update.getId());
		Assert.notNull(order, "can't find order with name " + update.getId());
		order.get().setPetId(update.getPetId());
		order.get().setQuantity(update.getQuantity());
		order.get().setStatus(update.getStatus());
		order.get().setShipDate(update.getShipDate());
		orderRepository.save(order.get());
		log.debug("order {} changes has been saved", update.getId());
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Order> findAll() {
		Iterable<Order> findAll = orderRepository.findAll();
		List<Order> myList = null;
		findAll.forEach(item -> myList.add(item) );
		return myList;
	}

	@Override
	public void deleteOrder(Order order) {
		orderRepository.delete(order);
	}

	

}
